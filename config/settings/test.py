from config.settings.base import *

DEBUG = True
ALLOWED_HOSTS = ['*']
DATABASES['default'] = env.db('DATABASE_URL', 'lol')
ALLOW_TEST_USER = env.bool('ALLOW_TEST_USER', default=False)
REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = (
    'rest_framework.authentication.SessionAuthentication',
    'rest_framework.authentication.BasicAuthentication',
)
