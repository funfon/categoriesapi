from config.settings.base import *
from corsheaders.defaults import default_headers

CORS_ALLOW_HEADERS = list(default_headers) + ['test-user']
DEBUG = True
SECRET_KEY = 'DJANGO_MINER_SECRET_KEY'
ALLOWED_HOSTS = ['*']
DATABASES['default'] = env.db('DATABASE_URL')
ALLOW_TEST_USER = env.bool('ALLOW_TEST_USER', default=False)

REST_FRAMEWORK['DEFAULT_PERMISSION_CLASSES'] = ('rest_framework.permissions.AllowAny',)

REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = (
    'rest_framework.authentication.SessionAuthentication',
    'rest_framework.authentication.BasicAuthentication',
)
