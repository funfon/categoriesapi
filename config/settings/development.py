from config.settings.base import *

env.read_env('.envs/.development')

DEBUG = env.bool('DEBUG')
SECRET_KEY = env.str('SECRET_KEY')
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', cast=str)
DATABASES['default'] = env.db()
