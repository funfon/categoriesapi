from django.db import models


class Category(models.Model):
    """
    Model for category
    """
    name = models.CharField(verbose_name='name of a category', blank=False, null=False, max_length=255, unique=True)
    level = models.IntegerField(verbose_name='level number', null=False)
    parent_name = models.CharField(verbose_name='parent name', null=True, blank=True, max_length=255)

    class Meta:
        db_table = 'category'

    @property
    def parents(self):
        """
        Parent for Category model
        :return: list of parents
        """
        return [{
            'id': parent.id,
            'name': parent.name,
        } for parent in self.get_parent_list(self.parent_name, [])]

    @property
    def children(self):
        """
        Children for Category model
        :return: list of children
        """
        return [{
            'id': children.id,
            'name': children.name,
        } for children in Category.objects.filter(parent_name=self.name)]

    @property
    def siblings(self):
        """
        Siblings for Category model
        :return: list of siblings
        """
        return [{
            'id': sibling.id,
            'name': sibling.name,
        } for sibling in Category.objects.filter(level=self.level, parent_name=self.parent_name).exclude(pk=self.pk)]

    def get_parent_list(self, parent_name, parent_list):
        if Category.objects.filter(name=self.parent_name).exists() and parent_name:
            parent = Category.objects.get(name=parent_name)
            parent_list.append(parent)
            self.get_parent_list(parent.parent_name, parent_list)

        return parent_list

    def __str__(self):
        """
        String representation of category
        :return: str
        """
        return f'{self.name}, level {self.level}'
