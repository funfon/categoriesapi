from rest_framework import serializers

from app.categories.models import Category


class CategorySerializer(serializers.ModelSerializer):
    """
    Serializer for creating Category model
    """

    class Meta:
        model = Category
        fields = ('id', 'name', 'level', 'parent_name',)

    def create(self, validated_data):
        """
        Create new Category object
        """
        return Category.objects.create(**validated_data)


class CategorySlugSerializer(serializers.ModelSerializer):
    """
    Serializer for displaying Category model
    """

    class Meta:
        model = Category
        fields = ('id', 'name', 'parents', 'children', 'siblings',)
