from django.urls import path, include

from rest_framework.routers import DefaultRouter

from app.categories.views import CategoryView

app_name = "categories"

router = DefaultRouter()

router.register(r'categories', CategoryView, basename='categories')

urlpatterns = [
    path(r'', include(router.urls)),
]
