import logging

from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework import authentication, permissions

from app.categories.models import Category
from app.categories.serializers import CategorySerializer, CategorySlugSerializer

logger = logging.getLogger(__name__)


class CategoryView(viewsets.ModelViewSet):
    """
    View to handle POST and GET requests to /categories/ endpoint
    """
    authentication_classes = (authentication.BasicAuthentication,)
    permission_classes = (permissions.AllowAny,)
    model = Category
    serializer_class = CategorySlugSerializer
    http_method_names = ['get', 'post']

    def create(self, request, *args, **kwargs):
        """
        Process POST request to /categories/ endpoint. Validate data and save it if no errors
        :return: processed status
        """
        data = request.data

        if data and isinstance(data, list):
            validated_data = []
            validated_data = [el for el in self.validate_request(data, 1, validated_data, '')
                              if not isinstance(el, list)]

            ser = CategorySerializer(data=validated_data, many=True)
            if ser.is_valid():
                ser.save()

                st = status.HTTP_201_CREATED

            else:
                logger.error(f'serializer error: {ser.errors}')
                st = status.HTTP_400_BAD_REQUEST

        else:
            logger.error(f'wrong request data: {data}')
            st = status.HTTP_400_BAD_REQUEST

        return Response(status=st)

    def get_queryset(self):
        """
        :return: validated queryset with parents, children and siblings
        """
        kwargs = self.request.parser_context.get('kwargs', None)
        if kwargs and kwargs.get('pk', None):
            qs = Category.objects.filter(pk=kwargs.get('pk'))

        else:
            qs = None

        return qs

    def validate_request(self, data, level, validated_data, parent_name):
        """
        Validate data from POST request
        :return: validated data for database
        """

        for element in data:
            name = element.get('name', None)
            validated_dict = {}

            if name:
                validated_dict['name'] = name
                validated_dict['level'] = level
                validated_dict['parent_name'] = parent_name

                validated_data.append(validated_dict)

                if element.get('children', None):
                    validated_data.append(self.validate_request(element.get('children'),
                                                                level + 1,
                                                                validated_data,
                                                                name))

        return validated_data
