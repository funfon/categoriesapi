# Categories API

Backend to handle categories tree

### Prerequisites

```
python 3.6+
docker
docker-compose
```

### Run using Docker

To build docker container

```
$ make build
```

To run docker container

```
$ make up
```

### Run as server

To run application create **venv** and install **requirements/dev.txt**

```
$ pip install -r requirements/dev.txt
$ python manage.py runserver
```

### Work with project

To fill database with test data use example.json file, **MUST BE LIST OF OBJECTS**, see example.json file in root directory.

Create **POST** request to **/categories/** endpoint with **example.json** data as body.

Create **GET** request with id **/categories/{id}/** to get data from database.

### Built with

* [Django](https://www.djangoproject.com/) - The web framework used
* [DRF](https://www.django-rest-framework.org/) - REST framework used

### Authors

* **[Ilia Levikov](https://www.instagram.com/i.levikov4/)** - *Initial work* - [funfon](https://bitbucket.org/funfon/)
