#!/bin/sh

set -o errexit
set -o nounset

python manage.py makemigrations --settings=config.settings.docker
python manage.py migrate --settings=config.settings.docker
python /app/manage.py collectstatic --noinput --settings=config.settings.docker
python /app/manage.py runserver 0.0.0.0:8000 --settings=config.settings.docker

