PROJECT_NAME?=app/categories

create_network:
	docker network create --driver bridge --subnet=172.28.0.0/16 --gateway 172.28.1.1 categories_api_docker_net &>/dev/null || true

build: create_network
	docker-compose -f local.yml build

build_no_cache: create_network
	docker volume rm categoriesapi_local_postgres_data
	docker-compose -f local.yml build --no-cache

up: create_network
	docker-compose -f local.yml up

clean:
	find . -regex '^.*\(__pycache__\|\.py[co]\)$$' -delete

create_user: create_network
	docker-compose -f local.yml run --rm django python manage.py createsuperuser --settings=config.settings.docker

shell_plus: create_network
	docker-compose -f local.yml run --rm django python manage.py shell_plus --settings=config.settings.docker

bash: create_network
	docker-compose -f local.yml run --rm django /bin/sh

